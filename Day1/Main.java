package com.company;

import java.util.Random;
import java.util.Scanner;

public class Main {

    public static Scanner scanner = new Scanner(System.in);
    public static Random rand  = new Random();

    public static void main(String[] args) {
        System.out.println("Enter the length of the array: ");
        int n = scanner.nextInt();
        int arrayOfInts[] = new int[n];

        System.out.println("Enter k: " );
        int k = scanner.nextInt();

        for(int i = 0; i< n; i++){
            arrayOfInts[i] = rand.nextInt(k);
        }


        System.out.println("Array: ");
        for(int x: arrayOfInts){
            System.out.print(x  + ", ");
        }


        for(int m = 0;m<arrayOfInts.length-1;m++){
            for(int p = m+1; p<arrayOfInts.length;p++){
                if((arrayOfInts[m] + arrayOfInts[p] )== k){
                    System.out.println( arrayOfInts[m] + " " + arrayOfInts[p]);
                }
            }
        }
    }
}
